# common-locale

### Provides functionality for displaying currencies and other data in a familiar form for users of all nationalities.

Install as a dependency in a project using `npm i git+https://git@bitbucket.org:dataliberated/common-locale.git` in your project's root.