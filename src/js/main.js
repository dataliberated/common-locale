import localeJSON from "../json/locale.json";

export function symbolFromISO( iso ) {
	if ( localeJSON.currencies[ iso ] != null ) {
		return localeJSON.currencies[ iso ];
	} else {
		return localeJSON.currencies._default;
	}
}
