import json from "rollup-plugin-json";
import { terser } from "rollup-plugin-terser";

export default [
	{
		input: "./src/js/main.js",
		output: {
			name: "common.locale",
			file: "./dist/common-locale.js",
			format: "iife",
			sourceMap: "inline"
		},
		plugins: [
			json()
		]
	},
	{
		input: "./src/js/main.js",
		output: {
			name: "common.locale",
			file: "./dist/common-locale.min.js",
			format: "iife",
			sourceMap: "inline"
		},
		plugins: [
			json(),
			terser()
		]
	}
];
